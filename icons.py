#!/usr/bin/python
# -*- coding: utf8 -*-

from os import listdir
from os.path import isfile, join
import re

LICENSE = '''
/**
 * CuteMaterialIcons - Google Material Design Icons for Qt
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
'''

BASEDIR = './build/repository'

CATEGORIES = [
  'action',
  'alert',
  'av',
  'communication',
  'content',
  'device',
  'editor',
  'file',
  'hardware',
  'image',
  'maps',
  'navigation',
  'notification',
  'places',
  'social',
  'toggle'
]

PATTERN = re.compile(r'ic_(\w+)_[0-9]+px\.svg')

def listFiles(path):
  return [f for f in listdir(path) if isfile(join(path, f))]

def definitions():
  lines = []

  for category in CATEGORIES:
    categoryDir = '{0}/{1}/svg/production'.format(BASEDIR, category)
    files = listFiles(categoryDir)

    for filename in files:
      match = PATTERN.match(filename)

      if match is not None:
        name = match.group(1)
        definition = '#define CUTE_MDI_{0} "{1}"'.format(name.upper(), name)

        if definition not in lines:
          print '> {0}'.format(name)

          lines.append(definition)

  return lines

def generateHeader():
  lines = definitions()

  print '> generating header file for {0} icons'.format(len(lines))

  with open('src/CuteIcons.h', 'w') as file:
    file.write(LICENSE.lstrip())
    file.write('\n')
    file.write('#ifndef CUTEICONS_H_\n')
    file.write('#define CUTEICONS_H_\n\n')
    file.write('\n'.join(lines))
    file.write('\n\n')
    file.write('#endif  // CUTEICONS_H_\n')

if __name__ == '__main__':
  generateHeader()
