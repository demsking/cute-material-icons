/**
 * CuteMaterialIcons - Google Material Design Icons for Qt
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CuteMaterialIcons.h"

#include <QFont>
#include <QIcon>
#include <QColor>
#include <QPixmap>
#include <QPainter>
#include <QFontDatabase>

bool CuteMaterialIcons::load() {
  return QFontDatabase::addApplicationFont(":/MaterialIcons") >= 0;
}

QFont CuteMaterialIcons::font() {
  QFont font;

  font.setFamily("Material Icons");

  return font;
}

QPixmap CuteMaterialIcons::pixmap(const char* icon, QColor color, int size) {
  QPixmap pix(size, size);
  pix.fill(Qt::transparent);

  QPainter painter(&pix);
  QFont painterFont = CuteMaterialIcons::font();

  painterFont.setPixelSize(size);
  painter.setFont(painterFont);
  painter.setPen(color);

  painter.drawText(0, size, icon);

  return pix;
}

QIcon CuteMaterialIcons::icon(const char* icon, QColor color, int size) {
  return QIcon(CuteMaterialIcons::pixmap(icon, color, size));
}
