/**
 * CuteMaterialIcons - Google Material Design Icons for Qt
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CUTEMATERIALICONS_H_
#define CUTEMATERIALICONS_H_

#include <QtCore/qglobal.h>

#if defined(CUTE_MATERIAL_ICONS)
  #define LIBCUTEMATERIALICONS_EXPORT Q_DECL_EXPORT
#else
  #define LIBCUTEMATERIALICONS_EXPORT Q_DECL_IMPORT
#endif

class QFont;
class QIcon;
class QColor;
class QPixmap;

class LIBCUTEMATERIALICONS_EXPORT CuteMaterialIcons {
  CuteMaterialIcons() = delete;

  public:
    static bool load();
    static QFont font();
    static QPixmap pixmap(const char* icon, QColor color, int size);
    static QIcon icon(const char* icon, QColor color, int size);
};

#endif  // CUTEMATERIALICONS_H_
