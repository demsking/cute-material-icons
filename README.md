# CuteMaterialIcons

Google Material Design Icons for Qt

## Usage

You can load the resources inside you main.cpp or at the main window constructor.

```cpp
#include "mainwindow.h++"
#include <QApplication>

#include "CuteMaterialIcons.h"

int main(int argc, char *argv[]) {
  QApplication app(argc, argv); // <- make sure resources are loaded after
                                //    the QApplication instantiation.
  CuteMaterialIcons::load();    // <- Load CuteMaterialIcons

  MainWindow window;

  window.show();

  return app.exec();
}
```

Then, in your code:

```cpp
#include <CuteIcons.h>
#include <CuteMaterialIcons.h>

...

QLabel label = new QLabel;

label->setFont(CuteMaterialIcons::font());
label->setText(CUTE_MDI_CLOSE);

// Use stylesheet to customize.
label->setStyleSheet("color: blue; font-size:30px;");

// You can resize the icon label
label->setFixedSize(30, 30);
```

To generate an icon, use `CuteMaterialIcons::icon()`.
```cpp
#include <CuteIcons.h>
#include <CuteMaterialIcons.h>
...

QIcon icon = CuteMaterialIcons::icon(CUTE_MDI_GAMEPAD, Qt::black, 24);
button->setIcon(icon);
```

To get a CuteMaterialIcons's pixmap, use `CuteMaterialIcons::pixmap()`.
```cpp
#include <CuteIcons.h>
#include <CuteMaterialIcons.h>
...

QPixmap pixmap = CuteMaterialIcons::pixmap(CUTE_MDI_CAMERA, Qt::black, 24);
```

## Build

```sh
# Create the cute-material-icons:ubuntu image
docker build . -t cute-material-icons:ubuntu

# Compile Setup
mkdir build
docker run -it --rm -v $(pwd):/home/dev --workdir /home/dev/build cute-material-icons:ubuntu cmake ..

# Compile CuteMaterialIcons
docker run -it --rm -v $(pwd):/home/dev --workdir /home/dev/build cute-material-icons:ubuntu make

# Packaging
docker run -it --rm -v $(pwd):/home/dev --workdir /home/dev/build cute-material-icons:ubuntu make artefact
```

## Contribute

Contributions to CuteMaterialIcons are welcome. Here is how you can contribute to cute-material-icons:

1. [Submit bugs or a feature request](https://gitlab.com/demsking/cute-material-icons/issues) and help us verify fixes as they are checked in
2. Write code for a bug fix or for your new awesome feature
3. Write test cases for your changes
4. [Submit merge requests](https://gitlab.com/demsking/cute-material-icons/merge_requests) for bug fixes and features and discuss existing proposals

## Versioning

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible API changes,
- `MINOR` version when you add functionality in a backwards-compatible manner, and
- `PATCH` version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the `MAJOR.MINOR.PATCH` format.

See [SemVer.org](https://semver.org/) for more details.

## License

Copyright (C) 2018 Sébastien Demanou.

Under the GNU General Public License.

Everyone is permitted to copy, modify and distribute CuteMaterialIcons. See
[LICENSE](https://gitlab.com/demsking/cute-material-icons/raw/master/LICENSE) file for more details.
